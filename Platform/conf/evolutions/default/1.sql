# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table post (
  id                            serial not null,
  title                         varchar(255),
  body                          varchar(255),
  id_startup                    varchar(255),
  created_at                    timestamp default now(),
  constraint pk_post primary key (id)
);

create table users (
  id                            serial not null,
  name                          varchar(255),
  phone                         varchar(255),
  address                       varchar(255),
  postalcode                    varchar(255),
  city                          varchar(255),
  state                         varchar(255),
  country                       varchar(255),
  accounttype                   varchar(255),
  email                         varchar(255),
  password                      varchar(255),
  fixed_line                    varchar(255),
  legal_entity                  varchar(255),
  vat                           varchar(255),
  registre_commerce             varchar(255),
  organisation                  varchar(255),
  image                         varchar(255),
  cin                           varchar(255),
  block_user                    boolean,
  startup                       varchar(255),
  admin_id                      integer,
  capital_social                integer,
  member                        boolean,
  constraint pk_users primary key (id)
);


# --- !Downs

drop table if exists post cascade;

drop table if exists users cascade;

