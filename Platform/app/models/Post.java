package models;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Model;
@Entity
public class Post extends Model {
	@Id
	public Integer id;
	public String title;
	public String body;
	public String id_startup;
	
	@Column(columnDefinition = "timestamp default now()")
	public Date created_at = new Date();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Date getCreated_at() {
		
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public String getId_startup() {
		return id_startup;
	}
	public void setId_startup(String id_startup) {
		this.id_startup = id_startup;
	}

	
	public static Finder<Integer,Post> find =new Finder<>(Post.class);




}
