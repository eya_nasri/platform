package models;
import java.util.HashSet;
import java.util.Set;
import java.lang.*;
import java.util.*;
import play.data.format.Formats.NonEmpty;
import play.data.validation.*;
import play.data.validation.Constraints.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.mindrot.jbcrypt.BCrypt;

import com.avaje.ebean.Model;


import play.data.*;
@Entity
@Table(name = "users")
public class Users extends Model {
	@Id
	
	public Integer id;
	
	@Column(length=255)
	@NonEmpty
	public String name;
	
	@Column(length=255)
	public String phone;
	
	@Column(length=255)
	public String address;
	
	@Column(length=255)
	public String postalcode;
	
	@Column(length=255)
	public String city;
	
	@Column(length=255)
	public String state;
	
	@Column(length=255)
	public String country;
	
	@Column(length=255)
	public String accounttype;
	
	@Email
	@Column(length=255)
	public String email;
	
	@Column(length=255)
	public String password;
	
	@Column(length=255)
	public String fixed_Line;
	
	@Column(length=255)
	public String legal_entity;
	
	@Column(length=255)
	public String vat;
	
	@Column(length=255)
	public String registre_commerce;
	
	@Column(length=255)
	public String organisation;
	
	@Column(length=255)
	public String image;
	
	@Column(length=255)
	public String cin;
	
	public boolean block_user;
	
	@Column(length=255)
	public String startup;
	
	public Integer admin_id;
	
	public Integer capital_social;
	
	public boolean member;
	
	
	
	
	
	
	
	public Integer getId() {
		return id;
	}







	public void setId(Integer id) {
		this.id = id;
	}







	public String getName() {
		return name;
	}







	public void setName(String name) {
		this.name = name;
	}







	public String getPhone() {
		return phone;
	}







	public void setPhone(String phone) {
		this.phone = phone;
	}







	public String getAddress() {
		return address;
	}







	public void setAddress(String address) {
		this.address = address;
	}







	public String getPostalcode() {
		return postalcode;
	}







	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}







	public String getCity() {
		return city;
	}







	public void setCity(String city) {
		this.city = city;
	}







	public String getState() {
		return state;
	}







	public void setState(String state) {
		this.state = state;
	}







	public String getCountry() {
		return country;
	}







	public Users(Integer id, String name, String phone, String address, String postalcode, String city, String state,
			String country, String accounttype, String email, String password, String fixedLine, String legal_entity,
			String vat, String registre_commerce, String organisation, String image, String cin, boolean block_user,
			String startup, Integer admin_id, Integer capital_social, boolean member) {
		super();
		this.id = id;
		this.name = name;
		this.phone = phone;
		this.address = address;
		this.postalcode = postalcode;
		this.city = city;
		this.state = state;
		this.country = country;
		this.accounttype = accounttype;
		this.email = email;
		this.password= password;
		this.fixed_Line = fixed_Line;
		this.legal_entity = legal_entity;
		this.vat = vat;
		this.registre_commerce = registre_commerce;
		this.organisation = organisation;
		this.image = image;
		this.cin = cin;
		this.block_user = block_user;
		this.startup = startup;
		this.admin_id = admin_id;
		this.capital_social = capital_social;
		this.member = member;
	}







	public Users(String email, String name, String password) {
	this.email=email;
	this.name=name;
	this.password=password;
	
	}







	public void setCountry(String country) {
		this.country = country;
	}







	public String getAccounttype() {
		return accounttype;
	}







	public void setAccounttype(String accounttype) {
		this.accounttype = accounttype;
	}







	public String getEmail() {
		return email;
	}







	public void setEmail(String email) {
		this.email = email;
	}







	public String getPassword() {
		return password;
	}







	public void setPassword(String password) {
		this.password = password;
	}







	public String getFixed_Line() {
		return fixed_Line;
	}







	public void setFixed_Line(String fixedLine) {
		this.fixed_Line = fixedLine;
	}







	public String getLegal_entity() {
		return legal_entity;
	}







	public void setLegal_entity(String legal_entity) {
		this.legal_entity = legal_entity;
	}







	public String getVat() {
		return vat;
	}







	public void setVat(String vat) {
		this.vat = vat;
	}







	public String getRegistre_commerce() {
		return registre_commerce;
	}







	public void setRegistre_commerce(String registre_commerce) {
		this.registre_commerce = registre_commerce;
	}







	public String getOrganisation() {
		return organisation;
	}







	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}







	public String getImage() {
		return image;
	}







	public void setImage(String image) {
		this.image = image;
	}







	public String getCin() {
		return cin;
	}







	public void setCin(String cin) {
		this.cin = cin;
	}







	public boolean isBlock_user() {
		return block_user;
	}







	public void setBlock_user(boolean block_user) {
		this.block_user = block_user;
	}

	public String getStartup() {
		return startup;
	}

	public void setStartup(String startup) {
		this.startup = startup;
	}

	public Integer getAdmin_id() {
		return admin_id;
	}
public void setAdmin_id(Integer admin_id) {
		this.admin_id = admin_id;
	}

public Integer getCapital_social() {
		return capital_social;
	}

public void setCapital_social(Integer capital_social) {
		this.capital_social = capital_social;
	}

public boolean isMember() {
		return member;
	}

public void setMember(boolean member) {
		this.member = member;
	}


	public static Finder<String,Users> find = new Finder<String,Users>(Users.class);
	 
	
	
	public static Users authenticate(String email, String password) {
	    //return  Users.find.where().eq("email", email).eq("password_hash", password).findUnique();
		
		//return find.where().eq("email", email)
	     //       .eq("password", password).findUnique();
	    
		  Users user = Users.find.where().eq("email", email).findUnique();
		    if (user != null && BCrypt.checkpw(password, user.password)) 
		    {
		      return user;
		    } else {
		      return null;
		      }
		      
	}
	}
