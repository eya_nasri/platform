package controllers;

import java.util.List;
import views.html.errors.*;
import javax.inject.Inject;

import models.Post;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.posts.*;

public class PostsController  extends Controller {
	@Inject
	FormFactory formFactory;
	
//for all posts	
public Result index()
{
List<Post> posts=Post.find.all();

return ok(index.render(posts));
}
//to create a post

public Result create()
{Form<Post> postForm=formFactory.form(Post.class);
	
return ok(create.render(postForm));}

//to save a post
public Result save()
{Form<Post> postForm=formFactory.form(Post.class).bindFromRequest();
if(postForm.hasErrors()) {
	flash("danger","Please Correct the Form Below ");
	return badRequest(create.render(postForm));
}

Post post =postForm.get();
post.save();
flash("success","Post Save Successfully");
return redirect(routes.PostsController.index());


}
//to edit a post
public Result edit(Integer id)
{
	Post post=Post.find.byId(id);
	if(post==null) {return notFound(views.html.errors._404.render());}
	Form<Post> postForm =formFactory.form(Post.class).fill(post);
	
	return ok(edit.render(postForm));
	
}
//to update
public Result update()
{ 
Form<Post> postForm =formFactory.form(Post.class).bindFromRequest();
if (postForm.hasErrors())
{
flash("danger","Please Correct the Form Below");
return badRequest(edit.render(postForm));
}
Post post= postForm.get();
//Post oldPost=Post.find.byId(post.id);
if(post==null) {
	
	flash("danger","Post Not Found");
	return notFound();}
//oldPost.title=post.title;
//oldPost.body=post.body;
//oldPost.created_at=post.created_at;
//oldPost.update();
post.update();
flash("success","Post updated successfully");

return ok();
}

//to destroy
public Result destroy(Integer id)
{Post post=Post.find.byId(id);
if(post==null) {
	flash("danger","Post Not Found");
	return notFound();}
flash("success","Post Deleted");
post.delete();
	
return ok();}
//or post details
public Result show(Integer id)
{Post post=Post.find.byId(id);
if (post==null)
{return notFound(_404.render());}

return ok(show.render(post));}

}



